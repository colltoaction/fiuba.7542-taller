# Ejercicio 3
# Mapa ASCII


## Introducción

En este trabajo práctico se hizo uso extenso de las características de Programación Orientada a Objetos presentes en C++ para resolver problemas planteados:

* manejo genérico de los objetos pertenecientes al mapa
* manejo genérico de las figuras que pueden representar el espacio ocupado por un objeto
* abstracción de la implementación de algoritmos complejos

Estas características quedarán más claras luego de mirar los diagramas de clase a continuación, que muestran los detalles de implementación de las clases `MapObject` y `Shape`.


## Componentes

### MapObject

![Jerarquía de objetos del mapa](diagramas/map_object_hierarchy.png)

Como se observa en el diagrama, múltiples objetos implementan la interfaz de la clase abstracta `MapObject`. Cada uno de ellos provee una abstracción de las características de un objeto del mapa, como puede ser el símbolo que lo identifica o el cálculo de su área edificada o arbolada.

Cabe destacar que `Map`, la clase que maneja los objetos del mapa y provee métodos de impresión, solo dialogará con los objetos a través de la interfaz `MapObject` de forma *polimórfica*. Esto permite reutilizar el mismo código para cualquier instancia concreta, como puede ser `TrafficLight`, y también agregar nuevas clases hijas sin afectar a la clase `Map`.


### Shape

La jerarquía de figuras, por otro lado, provee una abstracción sobre las formas posibles de los elementos del mapa. Si bien esto es cierto, la clase `Shape` no tiene ningún vínculo duro con el mapa. Solo a través de la *composición* con la clase `MapObject` es que se vinculan.

![Jerarquía de figuras](diagramas/shape_hierarchy.png)

`Shape` además encapsula comportamiento propio de las figuras y que puede variar mucho de figura a figura:

* el cálculo del área de un círculo sigue la clásica fórmula $\pi r^2$
* el cálculo del área de un polígono involucra a cada uno de los N puntos a través de una fórmula más compleja


### Buffer

`Buffer` provee el comportamiento necesario para imprimir figuras en un *lienzo* rectangular cuyos elementos son caracteres ASCII, en lugar de pixeles. Fue diseñada de manera de desligarse lo más posible de los detalles de implementación del trabajo práctico:

* se desliga de la implementación del mapa a través de la abstracción `Shape`
* se desliga de la impresión por salida estándar recibiendo por parámetro un objeto `std::ostream`

Cabe mencionar también que hace uso de *RAII* para crear y destruir de forma segura un array de caracteres en memoria dinámica. Se hace esto ya que su tamaño puede ser muy grande, dependiendo del ancho y alto especificados en su constructor.


### MapLoader

El encapsulamiento del *parseo* de figuras y objetos se lleva acabo en un solo lugar, `MapLoader`, y luego estos objetos son usados solo a través de sus abstracciones. Esta clase además agrega información al mapa como es la carga de referencias de edificios públicos.


## Conclusiones

Finalmente, y luego de observar el resultado del trabajo, se puede afirmar que la orientación a objetos ha permitido escribir código donde las responsabilidades de cada clase quedan bien definidas y desligadas lo más posible de otras clases.

Reflexionando sobre las características de C++ y el paradigma de *RAII*, surge la pregunta de si podría haberse evitado el uso de punteros en varios puntos del sistema. Debido a su arquitectura, es imposible usar recursos del stack y referencias en más de un lugar donde podría quererse. Se eligió para ciertos objetos livianos hacer una copia a la hora de retornarlos, pero para muchos otros se eligió usar punteros, con la desventaja de no poder aplicar *RAII*.
