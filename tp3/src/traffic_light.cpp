#include "map_object.h"
#include "shape.h"
#include "traffic_light.h"


TrafficLight::TrafficLight(Shape* shape)
    : MapObject(shape) {
}

char TrafficLight::Symbol() const {
    return '#';
}

floating_type TrafficLight::BuiltUpArea() const {
    return 0;
}

floating_type TrafficLight::GreenArea() const {
    return 0;
}
