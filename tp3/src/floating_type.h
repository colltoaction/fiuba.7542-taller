#ifndef FLOATING_TYPE_H
#define FLOATING_TYPE_H

#define LATITUDE_TO_M (floating_type(111319))
#define LONGITUDE_TO_M (floating_type(111131))
#define KM_TO_M (floating_type(1000))

/**
* Define el tipo de punto flotante a usar a lo largo del programa.
*/
typedef double floating_type;

#endif
