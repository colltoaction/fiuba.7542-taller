#include <iomanip>
#include <map>
#include <ostream>
#include <string>
#include <vector>
#include "buffer.h"
#include "map.h"
#include "map_object.h"


Map::~Map() {
    for (std::map< int, std::vector<MapObject*> >::iterator
            it = objects.begin();
         it != objects.end();
         ++it) {
        for (std::vector<MapObject*>::iterator it2 = it->second.begin();
             it2 != it->second.end();
             ++it2) {
            delete *it2;
        }
    }
}

void Map::Print(Buffer& buffer) const {
    for (std::map< int, std::vector<MapObject*> >::const_iterator
            it = objects.begin();
         it != objects.end();
         ++it) {
        for (std::vector<MapObject*>::const_iterator it2 = it->second.begin();
             it2 != it->second.end();
             ++it2) {
            buffer.Render((*it2)->shape(), (*it2)->Symbol());
        }
    }
}

void Map::PrintData(std::ostream& os) const {
    os << std::fixed << std::setprecision(0)
       << "Superficie total edificada: "
       << CalculateBuiltUpArea()
       << " metros cuadrados.\n"
       << "Superficie total arbolada: "
       << CalculateGreenArea()
       << " metros cuadrados.\n";
}

void Map::PrintReferences(std::ostream& os) const {
    for (std::map<char, std::string>::const_iterator
            it = references.begin();
         it != references.end();
         ++it) {
        os << (*it).first
           << ": "
           << (*it).second
           << '\n';
    }
}

void Map::Load(int layer, MapObject* object) {
    objects[layer].push_back(object);
}

void Map::AddReference(char const& id, std::string const& name) {
    references[id] = name;
}

floating_type Map::CalculateBuiltUpArea() const {
    floating_type area = 0;
    for (std::map< int, std::vector<MapObject*> >::const_iterator
            it = objects.begin();
         it != objects.end();
         ++it) {
        for (std::vector<MapObject*>::const_iterator it2 = it->second.begin();
             it2 != it->second.end();
             ++it2) {
            area += (*it2)->BuiltUpArea();
        }
    }

    return area;
}

floating_type Map::CalculateGreenArea() const {
    floating_type area = 0;
    for (std::map< int, std::vector<MapObject*> >::const_iterator
            it = objects.begin();
         it != objects.end();
         ++it) {
        for (std::vector<MapObject*>::const_iterator it2 = it->second.begin();
             it2 != it->second.end();
             ++it2) {
            area += (*it2)->GreenArea();
        }
    }

    return area;
}
