#include "floating_type.h"
#include "map_object.h"
#include "shape.h"
#include "water.h"


Water::Water(Shape* shape)
             : MapObject(shape) {
}

char Water::Symbol() const {
    return '-';
}

floating_type Water::BuiltUpArea() const {
    return 0;
}

floating_type Water::GreenArea() const {
    return 0;
}
