#include "boulevard.h"
#include "floating_type.h"
#include "map_object.h"
#include "shape.h"


Boulevard::Boulevard(Shape* shape)
                     : MapObject(shape) {
}

char Boulevard::Symbol() const {
    return 'b';
}

floating_type Boulevard::BuiltUpArea() const {
    return 0;
}

floating_type Boulevard::GreenArea() const {
    return 0;
}
