#ifndef TRAFFIC_LIGHT_H
#define TRAFFIC_LIGHT_H

#include "map_object.h"
#include "shape.h"


/**
* Representa un semáforo en el mapa.
*/
class TrafficLight: public MapObject {
public:
    explicit TrafficLight(Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;
};

#endif
