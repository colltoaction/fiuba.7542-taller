#ifndef MAP_OBJECT_H
#define MAP_OBJECT_H

#include "shape.h"
#include "floating_type.h"


/**
* Clase abstracta que representa un objeto en el mapa.
*/
class MapObject {
public:
    explicit MapObject(Shape* shape);
    virtual ~MapObject();

    /**
    * Retorna una figura que representa el espacio ocupado en el espacio por
    * este objeto.
    */
    Shape const& shape() const;

    /**
    * Calcula el área construida de este objeto. Deberá ser implementado por
    * las clases hijas.
    */
    virtual floating_type BuiltUpArea() const = 0;

    /**
    * Calcula el área arbolada de este objeto. Deberá ser implementado por las
    * clases hijas.
    */
    virtual floating_type GreenArea() const = 0;

    /**
    * Retorna el símbolo identificando este objeto en el mapa.
    */
    virtual char Symbol() const = 0;

private:
    Shape* shape_;
};

#endif
