#ifndef BLOCK_H
#define BLOCK_H

#include "map_object.h"
#include "shape.h"


/**
* Representa una manazana en el mapa.
*/
class Block: public MapObject {
public:
    explicit Block(Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;
};

#endif
