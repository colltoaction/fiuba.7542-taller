#include "block.h"
#include "map_object.h"
#include "shape.h"


Block::Block(Shape* shape)
             : MapObject(shape) {
}

char Block::Symbol() const {
    return 'm';
}

floating_type Block::BuiltUpArea() const {
    return 0;
}

floating_type Block::GreenArea() const {
    return 0;
}
