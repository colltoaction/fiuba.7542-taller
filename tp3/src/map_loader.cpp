#include <string>
#include <sstream>
#include <vector>
#include <stdexcept>
#include "block.h"
#include "boulevard.h"
#include "building.h"
#include "circle.h"
#include "map_loader.h"
#include "polygon.h"
#include "shape.h"
#include "traffic_light.h"
#include "tree.h"
#include "water.h"


MapLoader::MapLoader(std::istream& is)
                     : is(is) {
}

Shape* MapLoader::ReadCircle(std::istream& is) {
    floating_type lat, lon;
    floating_type radius;
    char skipComma;
    is >> lat >> skipComma >> lon >> skipComma >> radius;
    return new Circle(Point(lon, lat), radius);
}

Shape* MapLoader::ReadPolygon(std::istream& is) {
    std::vector<Point> points;
    floating_type lat, lon;
    char skipComma;
    while (is >> lat >> skipComma >> lon) {
        points.push_back(Point(lon, lat));
        is >> skipComma;
    }

    return new Polygon(points);
}

void MapLoader::Load(Map& map) {
    std::string line;
    char building_id = 'A';
    while (std::getline(is, line)) {
        std::string type;
        std::stringstream is(line);
        std::getline(is, type, ',');
        if (type == "edificio-publico") {
            std::string name;
            std::getline(is, name, ',');
            map.AddReference(building_id, name);
            map.Load(3, new Building(building_id++, ReadPolygon(is)));
        } else if (type == "edificio-privado") {
            map.Load(3, new Building('e', ReadPolygon(is)));
        } else if (type == "boulevard") {
            map.Load(2, new Boulevard(ReadPolygon(is)));
        } else if (type == "semaforo") {
            map.Load(3, new TrafficLight(ReadCircle(is)));
        } else if (type == "arbol") {
            map.Load(3, new Tree(ReadCircle(is)));
        } else if (type == "manzana") {
            map.Load(2, new Block(ReadPolygon(is)));
        } else if (type == "agua") {
            map.Load(1, new Water(ReadPolygon(is)));
        } else {
            throw std::logic_error("El archivo de entrada es incorrecto.");
        }
    }
}
