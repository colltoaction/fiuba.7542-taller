#ifndef WATER_H
#define WATER_H

#include "floating_type.h"
#include "map_object.h"
#include "shape.h"


/**
* Representa agua en el mapa.
*/
class Water: public MapObject {
public:
    explicit Water(Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;
};

#endif
