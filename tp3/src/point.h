#ifndef POINT_H
#define POINT_H

#include "floating_type.h"


/**
* Encapsula las coordenadas de un punto y provee métodos asociados a puntos.
*/
class Point {
public:
    Point(floating_type x, floating_type y);

    /**
    * Retorna la coordenada x del punto.
    */
    floating_type const& x() const;

    /**
    * Retorna la coordenada y del punto.
    */
    floating_type const& y() const;

    /**
    * Retorna la distancia a otro punto p.
    */
    floating_type Distance(Point const& p) const;

private:
    floating_type x_;
    floating_type y_;
};

#endif
