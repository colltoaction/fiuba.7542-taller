#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <iomanip>
#include "floating_type.h"
#include "point.h"
#include "polygon.h"
#include "shape.h"


Polygon::Polygon(std::vector<Point> points)
                 : points(points) {
}

// El cálculo del área asume que se trata de un polígono convexo
floating_type Polygon::Area() const {
    floating_type area(0);
    for (size_t i = 0, j = points.size() - 1;
         i < points.size();
         j = i++) {
        area += points[i].x() * points[j].y() -
                points[j].x() * points[i].y();
    }

    return 0.5f * LATITUDE_TO_M * LONGITUDE_TO_M * std::abs(area);
}

// Algoritmo PNPOLY
bool Polygon::Contains(Point const& p) const {
    bool contains = false;
    for (size_t i = 0, j = points.size() - 1;
         i < points.size();
         j = i++) {
        Point p1(points[i]);
        Point p2(points[j]);
        if (((p1.y() > p.y()) != (p2.y() > p.y())) &&
            (p.x() < (p2.x() - p1.x()) * 
                     (p.y() - p1.y()) / (p2.y() - p1.y()) + p1.x())) {
            contains = !contains;
        }
    }

    return contains;
}
