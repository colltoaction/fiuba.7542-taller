#ifndef MAP_BUFFER_H
#define MAP_BUFFER_H

#include "square.h"
#include "shape.h"


/**
* Buffer que servirá para imprimir un mapa en formato ASCII.
*/
class Buffer {
public:
    Buffer(Square const& limits, unsigned width, unsigned height);
    ~Buffer();

    /**
    * Inicializa el buffer con el caracter especificado para el fondo. De no
    * usarse, los caracteres de fondo serán memoria basura.
    */
    void Init(char background);

    /**
    * Dibuja una figura en el buffer usando el símbolo especificado.
    */
    void Render(Shape const& shape, char const& symbol);

    /**
    * Imprime el contenido el buffer en el stream.
    */
    void Print(std::ostream& os) const;

private:
    Square const& limits;
    unsigned width;
    unsigned height;

    /**
    * Almacena en contenido en crudo del buffer. Se usa un solo array y se lo
    * itera de forma especial para darle uso de matriz.
    * Se guarda en memoria dinámica ya que puede ser demasiado grande para la
    * memoria estática disponible.
    */
    char* buffer;

    /**
    * Calcula las coordenadas en latitud y longitud del centro del pixel (i,j).
    */
    Point GetPixelCoordinates(unsigned i, unsigned j) const;
};

#endif
