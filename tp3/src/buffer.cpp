#include <iomanip>
#include <ostream>
#include <vector>
#include "buffer.h"
#include "shape.h"
#include "square.h"


Buffer::Buffer(Square const& limits, unsigned width, unsigned height)
               : limits(limits)
               , width(width)
               , height(height)
               , buffer(new char[width * height]) {
}

Buffer::~Buffer() {
    delete[] buffer;
}

void Buffer::Init(char background) {
    for (unsigned i = 0; i < height; ++i) {
        for (unsigned j = 0; j < width; ++j) {
            buffer[i * width + j] = background;
        }
    }
}

void Buffer::Render(Shape const& shape, char const& symbol) {
    for (unsigned i = 0; i < height; ++i) {
        for (unsigned j = 0; j < width; ++j) {
            Point p(GetPixelCoordinates(j, i));
            if (shape.Contains(p)) {
                buffer[i * width + j] = symbol;
            }
        }
    }
}

void Buffer::Print(std::ostream& os) const {
    for (unsigned i = 0; i < height; ++i) {
        for (unsigned j = 0; j < width; ++j) {
            os << buffer[i * width + j];
        }

        os << '\n';
    }
}

Point Buffer::GetPixelCoordinates(unsigned i, unsigned j) const {
    floating_type x = ((floating_type)i + 0.5f) / (floating_type)width;
    floating_type y = ((floating_type)j + 0.5f) / (floating_type)height;
    return limits.PointFromProportion(x, y);
}
