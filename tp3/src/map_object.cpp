#include "floating_type.h"
#include "map_object.h"
#include "shape.h"


MapObject::MapObject(Shape* shape)
                     : shape_(shape) {
}

MapObject::~MapObject() {
    delete shape_;
}

Shape const& MapObject::shape() const {
    return *shape_;
}
