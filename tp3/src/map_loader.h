#ifndef MAP_LOADER_H
#define MAP_LOADER_H

#include <sstream>
#include "map.h"
#include "shape.h"


/**
* Interpreta la entrada de datos y crea los objetos del mapa correspondientes.
* Los añade al mapa para su uso posterior.
*/
class MapLoader {
public:
    explicit MapLoader(std::istream& is);

    /**
    * Lee el archivo de entrada, convirtiéndolo en objetos que serán agregados
    * al mapa pasado por parámetro.
    */
    void Load(Map& map);

private:
    std::istream& is;

    /**
    * Lee un círculo con el formato "latitud,longitud,radio".
    */
    Shape* ReadCircle(std::istream& is);

    /**
    * Lee un polígono representado por N puntos con el formato
    * "latitud,longitud".
    */
    Shape* ReadPolygon(std::istream& is);
};

#endif
