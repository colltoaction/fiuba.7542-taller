#ifndef BOULEVARD_H
#define BOULEVARD_H

#include "floating_type.h"
#include "map_object.h"
#include "shape.h"


/**
* Representa un boulevard en el mapa.
*/
class Boulevard: public MapObject {
public:
    explicit Boulevard(Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;
};

#endif
