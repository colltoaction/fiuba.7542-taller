#include "floating_type.h"
#include "map_object.h"
#include "shape.h"
#include "tree.h"


Tree::Tree(Shape* shape)
           : MapObject(shape) {
}

char Tree::Symbol() const {
    return '@';
}

floating_type Tree::BuiltUpArea() const {
    return 0;
}

floating_type Tree::GreenArea() const {
    return shape().Area();
}
