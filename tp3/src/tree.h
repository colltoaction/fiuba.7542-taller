#ifndef TREE_H
#define TREE_H

#include "floating_type.h"
#include "map_object.h"
#include "shape.h"


/**
* Representa un árbol en el mapa.
*/
class Tree: public MapObject {
public:
    explicit Tree(Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;
};

#endif
