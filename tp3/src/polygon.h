#ifndef POLYGON_H
#define POLYGON_H

#include <vector>
#include "floating_type.h"
#include "point.h"
#include "shape.h"


/**
* Representa un polígono convexo.
*/
class Polygon: public Shape {
public:
    explicit Polygon(std::vector<Point> points);
    floating_type Area() const;
    bool Contains(Point const& p) const;

private:
    std::vector<Point> points;
};

#endif
