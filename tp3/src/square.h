#ifndef SQUARE_H
#define SQUARE_H

#include "floating_type.h"
#include "point.h"


/**
* Representa un cuadrado con sus límites.
*/
class Square {
public:
    Square(floating_type t, floating_type b, floating_type l, floating_type r);

    /**
    * Toma dos números x e y entre 0 y 1, que corresponden a las proporciones
    * en cada eje del cuadrado de un punto buscado.
    * Retorna un nuevo punto que contiene las coordenadas dentro del cuadrado
    * del punto que tiene tales proporciones.
    */
    Point PointFromProportion(floating_type x, floating_type y) const;

private:
    floating_type t;
    floating_type b;
    floating_type l;
    floating_type r;
};

#endif
