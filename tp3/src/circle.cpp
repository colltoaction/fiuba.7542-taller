#include <vector>
#include <string>
#include <cmath>
#include "circle.h"
#include "floating_type.h"
#include "point.h"
#include "shape.h"


floating_type Circle::Area() const {
    return floating_type(M_PI * radius * radius);
}

bool Circle::Contains(Point const& p) const {
    floating_type distance_in_m(center.Distance(p));
    return distance_in_m <= radius;
}

Circle::Circle(Point center, floating_type radius)
        : center(center)
        , radius(radius * KM_TO_M) {
}
