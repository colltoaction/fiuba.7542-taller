#include <cmath>
#include "point.h"
#include "floating_type.h"


Point::Point(floating_type x, floating_type y)
             : x_(x)
             , y_(y) {
}

floating_type const& Point::x() const {
    return x_;
}

floating_type const& Point::y() const {
    return y_;
}

floating_type Point::Distance(Point const& p) const {
    return std::sqrt(
        (p.x_ - x_) * (p.x_ - x_) * 
        LATITUDE_TO_M * LATITUDE_TO_M +
        (p.y_ - y_) * (p.y_ - y_) * 
        LONGITUDE_TO_M * LONGITUDE_TO_M);
}
