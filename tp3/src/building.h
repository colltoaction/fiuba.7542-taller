#ifndef PRIVATE_BUILDING_H
#define PRIVATE_BUILDING_H

#include "map_object.h"
#include "shape.h"


/**
* Representa un edificio en el mapa.
*/
class Building: public MapObject {
public:
    Building(char id, Shape* shape);
    char Symbol() const;
    floating_type BuiltUpArea() const;
    floating_type GreenArea() const;

private:
	char id;
};

#endif
