#ifndef MAP_H
#define MAP_H

#include <string>
#include <vector>
#include <map>
#include "buffer.h"
#include "map_object.h"


/**
* Almacena los objetos del mapa y provee métodos de impresión y actualización.
*/
class Map {
public:
    ~Map();

    /**
    * Imprime el mapa sobre el buffer.
    */
    void Print(Buffer& buffer) const;

    /**
    * Imprime la información del áreas mapa en el stream.
    */
    void PrintData(std::ostream& os) const;

    /**
    * Imprime las referencias del mapa en el stream.
    */
    void PrintReferences(std::ostream& os) const;

    /**
    * Agrega un objeto al mapa, especificando en qué capa de rasterizado se
    * debe imprimir.
    */
    void Load(int layer, MapObject* object);

    /**
    * Agrega una referencia al mapa, identificada por un caracter y un nombre.
    */
    void AddReference(char const& id, std::string const& name);

private:
    /**
    * Calcula el área construida total en el mapa.
    */
    floating_type CalculateBuiltUpArea() const;

    /**
    * Calcula el área verde total en el mapa.
    */
    floating_type CalculateGreenArea() const;

    std::map< int, std::vector<MapObject*> > objects;
    std::map<char, std::string> references;
};

#endif
