#ifndef CIRCLE_H
#define CIRCLE_H

#include "point.h"
#include "shape.h"


/**
* Representa un círculo.
*/
class Circle: public Shape {
public:
    Circle(Point center, floating_type radius);
    floating_type Area() const;
    bool Contains(Point const& p) const;

private:
    Point center;
    floating_type radius;
};

#endif
