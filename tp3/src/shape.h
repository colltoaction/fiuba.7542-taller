#ifndef SHAPE_H
#define SHAPE_H

#include "point.h"
#include "floating_type.h"


/**
* Clase abstracta que representa una figura con sus propiedades.
*/
class Shape {
public:
    virtual ~Shape();

    /**
    * Retorna el área de la figura. Debe ser implementado por las clases hijas.
    */
    virtual floating_type Area() const = 0;

    /**
    * Indica si la figura contiene el punto.
    */
    virtual bool Contains(Point const& p) const = 0;
};

#endif
