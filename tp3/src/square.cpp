#include "floating_type.h"
#include "point.h"
#include "square.h"


Square::Square(floating_type t,
               floating_type b,
               floating_type l,
               floating_type r)
               : t(t)
               , b(b)
               , l(l)
               , r(r) {
}

Point Square::PointFromProportion(floating_type x, floating_type y) const {
    floating_type x2((r - l) * x + l);
    floating_type y2((b - t) * y + t);
    return Point(x2, y2);
}
