#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <syslog.h>
#include <vector>
#include "floating_type.h"
#include "map.h"
#include "map_loader.h"
#include "map_object.h"
#include "point.h"
#include "shape.h"
#include "square.h"

#define ARG_CONFIG_FILE 1
#define ARG_MAP_FILE 2


Square read_map_limits(std::istream& is) {
    floating_type t, b, l, r;
    is >> t
       >> b
       >> l
       >> r;
    return Square(t, b, l, r);
}

Buffer* read_buffer_config(std::istream& is, Square const& limits) {
    unsigned height, width;
    is >> height
       >> width;
    return new Buffer(limits, width, height);
}

int main(int argc, char const *argv[])
try {
    if (argc != 3) {
        std::cout << "Uso: ./tp <configuracion> <mapa>" << std::endl;
        return 1;
    }

    std::ifstream configFile(argv[ARG_CONFIG_FILE]),
                  mapFile(argv[ARG_MAP_FILE]);
    if (configFile.fail() || mapFile.fail()) {
        std::cerr << "Archivo inválido" << std::endl;
        return 1;
    }

    Square limits(read_map_limits(configFile));
    Buffer* buffer(read_buffer_config(configFile, limits));
    buffer->Init(' ');
    MapLoader loader(mapFile);
    Map map;
    loader.Load(map);
    map.PrintData(std::cout);
    map.Print(*buffer);
    buffer->Print(std::cout);
    map.PrintReferences(std::cout);
    delete buffer;
    return 0;
}
catch (std::exception const& e) {
    syslog(LOG_CRIT, "An exception wasn't handled: \"%s\".", e.what());
    return 1;
}
catch (...) {
    syslog(LOG_CRIT, "An unexpected exception wasn't handled.");
    return 1;
}
