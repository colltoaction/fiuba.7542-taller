#include "building.h"
#include "map_object.h"
#include "shape.h"


Building::Building(char id, Shape* shape)
                   : MapObject(shape)
                   , id(id) {
}

char Building::Symbol() const {
    return id;
}

floating_type Building::BuiltUpArea() const {
    return shape().Area();
}

floating_type Building::GreenArea() const {
    return 0;
}
