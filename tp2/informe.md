# Ejercicio 2
# Sistema de alerta temprana sobre el desbordamientos de ríos

## Flujo del sistema

Se presentan a continuación distintos diagramas de flujo del sistema en su conjunto. En cierto nivel de abstracción, los elementos que componen el sistema y que coinciden con los hilos de ejecución son:

* el cliente, que es un `conector`
* el servidor, representado por:
    * un hilo principal llamado `servidor`
    * un hilo `aceptador`, que deriva las conexiones entrantes
    * uno o más hilos `client proxy`, que atienden a los clientes

Estos diagramas pueden ayudar a comprender más fácilmente la elección de las estructuras programadas, que son explicadas a continuación..

![Ejemplo de manejo de un conector](diagramas/Ejemplo de manejo de un conector.png)

![Ejemplo de consultas](diagramas/Ejemplo de consultas.png)

## Cliente

El cliente es un programa que lee un flujo de datos a través de la entrada estándar, y luego de un mínimo preprocesamiento lo reenvía al servidor a cumpliendo el protocolo definido.

Se podría decir que este programa es sencillo en comparación con el programa servidor. Se destaca:

* comunicación por *sockets* unidireccional
* simplicidad de la entrada de datos
* programación monohilo

### Lectura de datos

La entrada de datos se realiza por la entrada estándar, y desde el comienzo del programa se conoce la cantidad máxima de datos que se almacenarán. Entre sus argumentos el programa recibe la cantidad de elementos sobre los cuáles se tomará la moda, los que son almacenados hasta que llegue el momento de su cálculo y luego descartados.

Dado que este cliente está pensado para su uso en un dispositivo embebido registrando niveles del río, queda la posibilidad de mejorar la performance y su uso de memoria usando por ejemplo un `array` en lugar de un `std::vector`. Conocer de antemano los datos a registrar permite un uso muy eficiente de un `array` evitando redimensiones.

### Envío de datos

El cliente toma cierta cantidad de mediciones definidas por parámetro y luego toma la moda. Este valor es enviado al servidor a través de un *socket* TCP y siguiendo un protocolo definido.

Se inicia la comunicación con el servidor mediante un mensaje especial del tipo "`conector seccion XX`" y luego se mantiene abierta hasta que se le envía el mensaje "`fin`". Durante el tiempo en que se encuentra conectado, el cliente envía al servidor actualizaciones de (la moda de) su valor constantemente, pero ignora cualquier comunicación entrante.

## Servidor

En contraposición al programa cliente, se destacan los puntos de mayor complejidad a la hora de escribir el código del servidor:

* comunicación por *sockets* bidireccional
* programación multihilo y concurrente

### Hilo principal

El servidor fue desarrollado usando múltiples hilos y usando técnicas de programación concurrente. Esto permite hacer un mejor uso de los recursos del sistema, en especial frente a operaciones I/O bloqueantes como lectura de la entrada estándar y comunicación por *sockets*.

El hilo principal es el encargado del *bootstrapping* del sistema, creando las estructuras principales e iniciando el hilo aceptador. Luego maneja el ciclo de vida del programa, esperando hasta recibir la señal de finalización "`q`" por la entrada estándar. En este punto se cierran las conexiones existentes, se unen los hilos y se cierra el programa.

### Hilo aceptador

Este hilo es casi tan sencillo como el hilo principal, pero su función es clave. Permite escuchar las conexiones entrantes y despacharlas rápidamente para su atención.

A la hora de diseñar este módulo se tuvo en cuenta la posibilidad de reusarlo en contextos futuros, y se decidió crear una interfaz `AcceptHandler` a través de la cuál interactuar. El aceptador recibe un objeto `AcceptHandler` interesado en ser notificado cuando se acepta una nueva conexión, y se desentiende de sus detalles de implementación. En este caso sabemos que será un objeto encargado de la creación de *proxies* corriendo cada uno en un hilo propio.

### Hilos *client proxy*

Para hacer la interacción con el cliente transparente se implementó `ClientProxy`. Esta clase contiene la lógica para comunicarse a través de *sockets* usando el protocolo de comunicación definido. Se eligió implementarla como un hilo ya que la comunicación con los clientes es de naturaleza bloqueante. Si existiera una conexión intermitente con un conector y la comunicación fuera lenta, los otros clientes no se verían perjudicados.

### Concurrencia

Se analizó el uso de datos de la aplicación y se decidió usar un patrón del tipo **monitor** para manejar el acceso concurrente a recursos compartidos.

Todos los hilos *proxy* hacen uso compartido de un objeto de la clase `ConcurrentData`, que almacena los datos del sistema. Esta clase encierra un `std::map` y monitorea el acceso haciendo uso de *locks* de lectura y escritura. Se eligió usar este tipo de *lock* para optimizar el uso del recurso compartido, ya que múltiples clientes pueden leer esta información en simultáneo.

Dado que los datos de cada sección pueden ser retornados para su lectura y escritura independientemente de otras secciones, se decidió encapsularlos en la clase `ConcurrentDataSection`, con sus propios *locks* de lectura y escritura.

### *Sockets*

Se crearon dos simples clases para el manejo de *sockets*: `Socket` y `ListenerSocket` simplemente encapsulan las funciones de manejo de *sockets* de la librería estándar de `C` para su uso orientado a objetos, en especial en relación a la adquisición y liberación de recursos.

## Abstracciones reutilizables

Por último, se mencionarán brevemente las estructuras pensadas para su reutilización en contextos más allá del presente trabajo:

* `Action<T>`: una interfaz que encapsula una acción a realizar sobre un objeto de tipo `T`. Suplementa la falta de soporte para programación funcional en `C++98`, y en caso de actualizarse a `C++11` probablemente se reemplazaría por `std::function` u otro tipo nativo
* `RWLock`, `LockRead`, `LockWrite`: estas clases encapsulan `pthread_rwlock` de `C` y proveen una interfaz RAII sobre las operaciones `lock` y `unlock`
* `Socket`: como ya se mencionó, esta clase es un *thin wrapper* alrededor de las funciones de `C` de manejo de *sockets*
* `Thread`: al igual que `Socket`, esta clase es también un *thin wrapper* alrededor de funciones de `C`, en este caso de la biblioteca `pthreads`
