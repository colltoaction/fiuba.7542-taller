#include <string>
#include "common_lock_read.h"
#include "common_lock_write.h"
#include "common_rwlock.h"
#include "server_concurrent_data.h"


ConcurrentDataSection::ConcurrentDataSection(std::string const& name)
                                             : name(name) {
}

void ConcurrentDataSection::Update(std::string const& level,
                                   std::string const& flow) {
    LockWrite l(rwlock);
    this->level = level;
    this->flow = flow;
}

std::string ConcurrentDataSection::ToString() {
    LockRead l(rwlock);
    return level.empty() ? "" :
        "seccion " + name + " nivel " + level + " caudal " + flow + "\n";
}
