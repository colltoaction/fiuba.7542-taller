#ifndef SERVER_LISTENER_SOCKET_H
#define SERVER_LISTENER_SOCKET_H

#include "common_socket.h"

class ListenerSocket {
public:
    explicit ListenerSocket(const char* port);
    ~ListenerSocket();
    void Listen() const;
    void Close() const;

	/**
	* Accepts a new connection and creates a new socket in peerskt.
	* Returns -1 if the accept operation failed and the content of
	* peerskt is undefined.
	*/
    int Accept(Socket*& peerskt) const;
private:
    int skt;
    struct addrinfo *address_info;
};

#endif // SERVER_LISTENER_SOCKET_H
