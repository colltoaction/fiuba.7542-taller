#include <algorithm>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include "client_measurement.h"


Measurement::Measurement(unsigned int level, unsigned int flow)
                         : level(level)
                         , flow(flow) {
}

Measurement::Measurement(std::string const& line) {
    std::string ignored_names;
    std::istringstream(line)
        >> std::noskipws
        >> ignored_names >> std::ws
        >> level >> std::ws
        >> ignored_names >> std::ws
        >> flow;
}

unsigned int Measurement::Level() const {
    return level;
}

unsigned int Measurement::Flow() const {
    return flow;
}

unsigned int find_max_index(std::map<unsigned int, unsigned int>& count_map) {
    unsigned int max_count = 0, max_index = 0;
    for (std::map<unsigned int, unsigned int>::const_iterator
            i = count_map.begin();
         i != count_map.end();
         ++i) {
        if (i->second > max_count) {
            max_index = i->first;
            max_count = i->second;
        } else if (i->second == max_count) {
            max_index = std::max(max_index, i->first);
        }
    }

    return max_index;
}

Measurement* Measurement::Mode(std::vector<Measurement> const& measurements) {
    std::map<unsigned int, unsigned int> level_count, flow_count;
    for (std::vector<Measurement>::const_iterator i = measurements.begin();
         i != measurements.end();
         ++i) {
        level_count[i->Level()]++;
        flow_count[i->Flow()]++;
    }

    return new Measurement(find_max_index(level_count),
                           find_max_index(flow_count));
}
