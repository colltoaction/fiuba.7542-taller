#include "common_lock_write.h"
#include "common_rwlock.h"


LockWrite::LockWrite(RWLock& rwlock)
                     : rwlock(rwlock) {
    rwlock.LockWrite();
}

LockWrite::~LockWrite() {
    rwlock.Unlock();
}
