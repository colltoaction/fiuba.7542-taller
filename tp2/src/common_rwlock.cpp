#include <pthread.h>
#include "common_rwlock.h"


RWLock::RWLock() {
	pthread_rwlock_init(&rwlock, NULL);
}

RWLock::~RWLock() {
	pthread_rwlock_destroy(&rwlock);
}

void RWLock::LockRead() {
	pthread_rwlock_rdlock(&rwlock);
}

void RWLock::LockWrite() {
	pthread_rwlock_wrlock(&rwlock);
}

void RWLock::Unlock() {
	pthread_rwlock_unlock(&rwlock);
}
