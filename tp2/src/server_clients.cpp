#include <vector>
#include "server_clients.h"
#include "server_client_proxy.h"


Clients::Clients() : data() {
}

Clients::~Clients() {
    for (std::vector<ClientProxy*>::iterator it = threads.begin();
         it != threads.end();
         ++it) {
        delete *it;
    }
}

void Clients::CloseConnections() {
    for (std::vector<ClientProxy*>::iterator it = threads.begin();
         it != threads.end();
         ++it) {
        (*it)->CloseConnection();
        (*it)->Join();
    }
}

void Clients::Handle(Socket* peerskt) {
    // ClientProxy deletes peerskt
    ClientProxy* clientThread = new ClientProxy(peerskt, data);
    clientThread->OpenConnection();
    clientThread->Start();
    threads.push_back(clientThread);
}
