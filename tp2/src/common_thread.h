#ifndef COMMON_THREAD_H
#define COMMON_THREAD_H

class Thread {
public:
    virtual ~Thread() { }
    void Start();
    void Join();
protected:
    virtual void ThreadMain() = 0;
private:
    pthread_t thread;
    static void* StartRoutine(void* arg);
};

#endif // COMMON_THREAD_H
