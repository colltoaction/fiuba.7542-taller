#ifndef COMMON_RWLOCK_H
#define COMMON_RWLOCK_H

#include <pthread.h>

class RWLock {
public:
    RWLock();
    ~RWLock();
    void LockRead();
    void LockWrite();
    void Unlock();
private:
    pthread_rwlock_t rwlock;
};

#endif // COMMON_RWLOCK_H
