#ifndef SERVER_CLIENT_PROXY_H
#define SERVER_CLIENT_PROXY_H

#include <string>
#include "common_socket.h"
#include "common_thread.h"
#include "server_concurrent_data.h"

class ClientProxy: public Thread {
public:
    /**
    * This object will handle the whole peerskt lifecycle: opening, closing,
    * destroying.
    * It would be a good idea to use C++11 unique_ptr.
    */
    ClientProxy(Socket* peerskt, ConcurrentData& data);
    ~ClientProxy();
    void OpenConnection();
    void CloseConnection();
protected:
    void ThreadMain();
private:
    Socket* peerskt;
    ConcurrentData& data;
    ConcurrentDataSection* section; // Can be null at first

    /**
    * Handles the line read and returns a boolean indicating if the read
    * operation should continue
    */
    bool HandleLine(std::string const& line);
    void ReadSocket();
};

#endif // SERVER_CLIENT_PROXY_H
