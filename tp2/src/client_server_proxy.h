#ifndef CLIENT_SERVER_PROXY_H
#define CLIENT_SERVER_PROXY_H

#include <netdb.h>
#include <string>
#include <vector>
#include "client_measurement.h"
#include "common_socket.h"

class ServerProxy {
public:
    ServerProxy(const char* host,
                const char* port,
                unsigned int& mode);
    ~ServerProxy();
    void Connect(std::string const& section) const;
    void Close() const;
    void Send(Measurement const& measurement) const;
    void ReadLine(std::string const& line);
private:
    const Socket* socket;
    struct addrinfo* address_info;
    unsigned int& mode;
    std::vector<Measurement> measurements;
};

#endif // CLIENT_SERVER_PROXY_H
