#include "common_action.h"
#include "common_socket.h"
#include "server_socket_section_sender.h"

SocketSectionSender::SocketSectionSender(Socket const& skt)
                                         : skt(skt) {
}

void SocketSectionSender::Run(ConcurrentDataSection& section) {
    skt.Send(section.ToString());
}
