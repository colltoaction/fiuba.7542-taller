#ifndef SERVER_CLIENTS_H
#define SERVER_CLIENTS_H

#include <vector>
#include "server_acceptor_thread.h"
#include "server_client_proxy.h"
#include "server_concurrent_data.h"

class Clients: public AcceptHandler {
public:
    Clients();
    ~Clients();
    void CloseConnections();

    /**
	* Implements the AcceptHandler interface.
	* Receives a new connection from the acceptor thread and starts a new
	* connection with the client.
    */
    void Handle(Socket* peerskt);
private:
    ConcurrentData data;
    std::vector<ClientProxy*> threads;
};

#endif // SERVER_CLIENTS_H
