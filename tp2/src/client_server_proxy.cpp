#include <cstring>
#include <netdb.h>
#include <sstream>
#include <string>
#include <sys/socket.h>
#include "client_server_proxy.h"


ServerProxy::ServerProxy(const char* host,
                         const char* port,
                         unsigned int& mode)
                         : mode(mode) {
    struct addrinfo hints;

    std::memset(&hints, 0, sizeof(hints));
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_family = AF_INET;

    getaddrinfo(host, port, &hints, &address_info);
    socket = new Socket(*address_info);
}

ServerProxy::~ServerProxy() {
    delete socket;
    freeaddrinfo(address_info);
}

void ServerProxy::Connect(std::string const& section) const {
    std::ostringstream oss;
    socket->Connect(*address_info);
    oss << "conector seccion "
        << section
        << "\n";
    socket->Send(oss.str());
}

void ServerProxy::Close() const {
    socket->Send("fin\n");
    socket->Close();
}

void ServerProxy::Send(Measurement const& measurement) const {
    std::ostringstream oss;
    oss << "actualizar nivel "
        << measurement.Level()
        << " caudal "
        << measurement.Flow()
        << "\n";
    socket->Send(oss.str());
}

void ServerProxy::ReadLine(std::string const& line) {
    measurements.push_back(Measurement(line));
    if (measurements.size() >= mode) {
        Measurement* modeMeasurement(Measurement::Mode(measurements));
        Send(*modeMeasurement);
        delete modeMeasurement;
        measurements.clear();
    }
}
