#include "common_lock_read.h"
#include "common_rwlock.h"


LockRead::LockRead(RWLock& rwlock)
                   : rwlock(rwlock) {
    rwlock.LockRead();
}

LockRead::~LockRead() {
    rwlock.Unlock();
}
