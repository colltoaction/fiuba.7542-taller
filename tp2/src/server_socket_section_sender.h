#ifndef SERVER_SOCKET_SECTION_SENDER_H
#define SERVER_SOCKET_SECTION_SENDER_H

#include "common_action.h"
#include "server_concurrent_data_section.h"

class SocketSectionSender: public Action<ConcurrentDataSection&> {
public:
    explicit SocketSectionSender(Socket const& skt);
    void Run(ConcurrentDataSection& section);
private:
    Socket const& skt;
};

#endif // SERVER_SOCKET_SECTION_SENDER_H
