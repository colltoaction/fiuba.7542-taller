#ifndef COMMON_ACTION_H
#define COMMON_ACTION_H

template<class T>
class Action {
public:
	virtual void Run(T) = 0;
};

#endif // COMMON_ACTION_H
