#include <iostream>
#include <sstream>
#include <string>
#include "client_server_proxy.h"


int main(int argc, char const *argv[]) {
    if (argc != 5) {
        std::cout << "Uso: ./conector <host> <puerto> <seccion> <N>\n";
        return 1;
    }

    const char* host = argv[1];
    const char* port = argv[2];
    const char* section = argv[3];
    unsigned int mode;
    std::istringstream(argv[4]) >> mode;

    ServerProxy server(host, port, mode);
    server.Connect(section);
    for (std::string line; std::getline(std::cin, line);) {
        if (!line.empty()) {
            server.ReadLine(line);
        }
    }
    server.Close();

    return 0;
}
