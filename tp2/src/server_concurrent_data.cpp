#include <map>
#include <string>
#include <utility>
#include "common_lock_read.h"
#include "common_lock_write.h"
#include "common_rwlock.h"
#include "server_concurrent_data.h"


ConcurrentDataSection& ConcurrentData::GetSection(std::string const& name) {
    LockWrite l(rwlock);
    return data.insert(
        make_pair(name, ConcurrentDataSection(name))).first->second;
}

void ConcurrentData::ApplyToAll(Action<ConcurrentDataSection&>& action) {
    LockRead l(rwlock);
    for (std::map<std::string, ConcurrentDataSection>::iterator
            it = data.begin();
         it != data.end();
         ++it) {
        action.Run(it->second);
    }
}

void ConcurrentData::ApplyTo(std::string const& name,
                             Action<ConcurrentDataSection&>& action) {
    ConcurrentDataSection& section(GetSection(name)); // GetSection has a lock
    LockRead l(rwlock);
    action.Run(section);
}
