#ifndef COMMON_LOCK_WRITE_H
#define COMMON_LOCK_WRITE_H

#include "common_rwlock.h"

class LockWrite {
public:
    explicit LockWrite(RWLock& rwlock);
    ~LockWrite();
private:
    RWLock& rwlock;
};

#endif // COMMON_LOCK_WRITE_H
