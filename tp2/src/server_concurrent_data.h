#ifndef SERVER_CONCURRENT_DATA_H
#define SERVER_CONCURRENT_DATA_H

#include <map>
#include <string>
#include "common_action.h"
#include "common_rwlock.h"
#include "server_concurrent_data_section.h"

class ConcurrentData {
public:
    ConcurrentDataSection& GetSection(std::string const& name);

    /**
    * Applies a callback to each element in a thread-safe way.
    */
    void ApplyToAll(Action<ConcurrentDataSection&>& action);

    /**
    * Applies a callback to a single element in a thread-safe way.
    */
    void ApplyTo(std::string const& name,
                 Action<ConcurrentDataSection&>& action);
private:
    std::map<std::string, ConcurrentDataSection> data;
    RWLock rwlock;
};

#endif // SERVER_CONCURRENT_DATA_H
