#ifndef CLIENT_MEASUREMENT_H
#define CLIENT_MEASUREMENT_H

#include <string>
#include <vector>

class Measurement {
public:
    explicit Measurement(std::string const& line);
    unsigned int Level() const;
    unsigned int Flow() const;
    static Measurement* Mode(std::vector<Measurement> const& measurements);
private:
    unsigned int level;
    unsigned int flow;
    Measurement(unsigned int level, unsigned int flow);
};

#endif // CLIENT_MEASUREMENT_H
