#ifndef SERVER_ACCEPTOR_THREAD_H
#define SERVER_ACCEPTOR_THREAD_H

#include <string>
#include <vector>
#include "common_thread.h"
#include "server_listener_socket.h"

class AcceptHandler {
public:
	virtual void Handle(Socket* socket) = 0;
};

class AcceptorThread: public Thread {
public:
    AcceptorThread(const char* port, AcceptHandler& handler);
    void StartListening();
    void StopListening();
protected:
    void ThreadMain();
private:
    ListenerSocket skt;
    AcceptHandler& handler;
};

#endif // SERVER_ACCEPTOR_THREAD_H
