#ifndef SERVER_CONCURRENT_DATA_SECTION_H
#define SERVER_CONCURRENT_DATA_SECTION_H

#include <string>
#include "common_rwlock.h"

class ConcurrentDataSection {
public:
    explicit ConcurrentDataSection(std::string const& name);
    void Update(std::string const& level, std::string const& flow);
    std::string ToString();
private:
    std::string name;
    std::string level;
    std::string flow;
    RWLock rwlock;
};

#endif // SERVER_CONCURRENT_DATA_SECTION_H
