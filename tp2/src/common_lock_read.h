#ifndef COMMON_LOCK_READ_H
#define COMMON_LOCK_READ_H

#include "common_rwlock.h"

class LockRead {
public:
    explicit LockRead(RWLock& rwlock);
    ~LockRead();
private:
    RWLock& rwlock;
};

#endif // COMMON_LOCK_READ_H
