#!/bin/bash

testcase=$1
echo "[[[ test case: $testcase ]]]"
port=$(./free_port.sh 3000)

rm -Rf runenv/*
if [ $? -ne 0 ] 
then
  echo "FAIL: rm failed"
  exit 1
fi
mkdir runenv/expected

cp ../bin/client ../bin/server runenv

cp -f $testcase/in.zip runenv/
cp -f $testcase/out.zip runenv/expected
cp -f run.sh runenv/

cd runenv
unzip in.zip >/dev/null

cd expected
unzip out.zip >/dev/null
cd ..

./run.sh localhost $port "$2" single
if [ $? -ne 0 ] 
then
  echo "FAIL: run.sh failed"
  exit 1
fi

for s in $(cd expected && ls *.out __*)
do 
    diff $s expected/$s
    if [ $? -ne 0 ] 
    then
      echo "FAIL: $s"
      exit 1
    fi  
done

for f in $(ls valgrind_*.out)
do
	# cat $f | grep -q "All heap blocks were freed -- no leaks are possible"
	cat $f | grep -q "ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)"
	if [ $? -ne 0 ]
	then
		echo "Valgrind $f... ERROR"
		exit 1
	else
		echo "Valgrind $f... OK"
	fi
done

cd ..
sleep 1
