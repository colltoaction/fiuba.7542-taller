# Ejercicio 1 
# Inspección de Tuberías Industriales


## Flujo del programa

Presentamos a continuación el diagrama de flujo del programa, que consiste en un
análisis de mediciones de uno o más sensores en una tubería industrial. Creemos
que este diagrama puede ayudar a comprender más fácilmente la elección de las
estructuras programadas, que son explicadas a continuación.

![Flow](flow-tp.png)


## TAD de lectura de mediciones `measurements_reader`

Se eligió encapsular la lectura del archivo de mediciones en un tipo de dato ya
que presenta funcionalidad y datos muy bien definidos:

* lee el archivo de mediciones haciendo las conversiones de endianness
* necesarias para evitar leer el archivo completo, provee un iterador interno
* para iterar las mediciones para cada instante `t` lee las mediciones de los
* `n` sensores que se hayan especificado en la cabecera del archivo de
* mediciones, y luego lo pasa a la función de iteración provee la información
* leída de las cabeceras del archivo, como la velocidad de flujo o la cantidad
* de sensores


## TAD de lectura del mapa `pig_map`

Este tipo de dato encapsula la lectura del archivo de tuberías, que
interpretamos como un mapa y modelamos como un grafo. El grafo se implementó de
forma enlazada ya que proveía de forma sencilla métodos para la iteración del
camino. `pig_map` abstrae las particularidades del archivo de tuberías, como que
contiene solo uno de los dos sentidos en los enlaces entre nodos, y almacena la
información de los nombres y las distancias.


## TAD de lectura e iteración del camino `pig_path`

Para continuar con el encapsulamiento del comportamiento, escribimos el TAD de
lectura e iteración del camino. Es importante destacar que dado que cada archivo
tiene un formato distinto, es conveniente que su interpretado quede en manos de
un TAD. En caso de necesitarse cambiar un formato de archivo, se podrá editar el
TAD acordemente sin afectar a otros módulos que haga uso del mismo. Un ejemplo
claro sería que se decidiera cambiar la endianness de los archivos de entrada
binarios.

Este TAD lee el camino que recorrió el Pig desde el archivo especificado y luego
proporciona acceso conveniente al mismo a través de una serie de funciones como
`current` y `next` que dan funcionalidad básica de iteración.


## TAD de análisis de tuberías `path_analyzer`

Este tipo de dato es el encargado de unir la información del camino del Pig a la
información de las mediciones hechas por el Pig. Provee una función para
procesar las mediciones en el instante `t` como especifica el lector de
mediciones, y en cada iteración analiza los datos de los sensores y almacena
información que pueda necesitar más adelante.

Es también el encargado de llevar el control de la ubicación en la que nos
encontremos: usa la información del camino del pig y lleva registro de en qué
sección de camino se encuentra a la hora de hacer una medición.


## TAD de análisis de datos del sensor `sensor`

A la hora de analizar la información de los sensores, encontramos útil crear un
tipo de dato `sensor`. Esto se debe a que cada sensor debe analizar su historia
por separado y encontrar los puntos de ruptura y de corrosión.

Encapsular este tipo de comportamiento nos permite que tipos como `path_analyze`
se encarguen de hacer una sola tarea y de hacerla bien.


## Módulo de funciones para conversión de endianness

Se desarrolló también un módulo de funciones auxiliares para lectura de números
en formato *big endian*, que es el formato que tenía el archivo binario de
mediciones. Dado que son funciones genéricas que podrían usarse en el contexto
de muchos programas distintos, separamos un módulo para estas.
