#include <stdint.h>
#include "endianness.h"

const int __is_bigendian_i = 1;
#define is_bigendian() ( (*(char*)&__is_bigendian_i) == 0 )

uint32_t read_big_endian_uint32_t(uint8_t* n) {
    uint32_t i;
    char *p = (char *)&i;

    if (is_bigendian()) {
        p[0] = n[0];
        p[1] = n[1];
        p[2] = n[2];
        p[3] = n[3];
    } else {
        p[0] = n[3];
        p[1] = n[2];
        p[2] = n[1];
        p[3] = n[0];
    }

    return i;
}

uint16_t read_big_endian_uint16_t(uint8_t* n) {
    uint16_t i;
    char *p = (char *)&i;

    if (is_bigendian()) {
        p[0] = n[0];
        p[1] = n[1];
    } else {
        p[0] = n[1];
        p[1] = n[0];
    }

    return i;
}
