#ifndef _ENDIANNESS_H
#define _ENDIANNESS_H

#include <stdint.h>

uint16_t read_big_endian_uint16_t(uint8_t* n);

uint32_t read_big_endian_uint32_t(uint8_t* n);

#endif // _ENDIANNESS_H
