#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "endianness.h"
#include "measurements_reader.h"

#define MAX_SENSOR_COUNT 10
#define MAX_NORMAL_MEASUREMENT = 0x0099
#define MIN_CORROSION_MEASUREMENT = 0x00A0
#define MIN_CORROSION_MEASUREMENT_DISTANCE = 0.5
#define MIN_LEAK_MEASUREMENT = 0x00FF

bool measurements_read_headers(measurements_reader_t* r) {
    size_t elemCount = 3;
    size_t elemBytes = sizeof(uint32_t);
    uint8_t buf[elemCount][elemBytes];
    if (fread(&buf, elemBytes, elemCount, r->fileptr) != elemCount) {
        return false;
    }

    r->header.fluxSpeed = read_big_endian_uint32_t(buf[0]);
    r->header.censusSpeed = read_big_endian_uint32_t(buf[1]);
    r->header.sensorCount = read_big_endian_uint32_t(buf[2]);
    return true;
}

bool measurements_reader_iterate(measurements_reader_t* r,
                                 measurement_iter_t iter,
                                 void* extra) {
    size_t elemCount = r->header.sensorCount;
    size_t elemBytes = sizeof(measurement_t);
    uint8_t buf[elemCount][elemBytes];
    size_t read;
    while ((read = fread(&buf, elemBytes, elemCount, r->fileptr))
            == elemCount) {
        measurement_t measurements[MAX_SENSOR_COUNT];
        for (size_t i = 0; i < elemCount; ++i) {
            measurements[i] = read_big_endian_uint16_t(buf[i]);
        }

        iter(measurements, elemCount, extra);
    }

    return read <= 0;
}

measurements_reader_t* measurements_reader_create(FILE* fileptr) {
    measurements_reader_t* r = malloc(sizeof(measurements_reader_t));
    r->fileptr = fileptr;
    if (!measurements_read_headers(r)) {
        measurements_reader_destroy(r);
        return NULL;
    }

    return r;
}

uint32_t measurements_reader_census_speed(measurements_reader_t* r) {
    return r->header.censusSpeed;
}

uint32_t measurements_reader_flux_speed(measurements_reader_t* r) {
    return r->header.fluxSpeed;
}

uint32_t measurements_reader_sensor_count(measurements_reader_t* r) {
    return r->header.sensorCount;
}

void measurements_reader_destroy(measurements_reader_t* r) {
    free(r);
}
