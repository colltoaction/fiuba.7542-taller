#ifndef _PIG_MAP_H
#define _PIG_MAP_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define LINE_MAX_LEN 50
#define NODE_NAME_MAX_LEN 21

typedef struct _pig_node_t pig_node_t;

typedef struct {
    pig_node_t* startNode;
    pig_node_t* endNode;
    unsigned int distance;
} pig_link_t;

struct _pig_node_t {
    char name[NODE_NAME_MAX_LEN];
    pig_link_t** links;
    size_t linksLength;
    size_t linksSize;
};

typedef struct {
    pig_node_t** nodes;
    size_t nodesLength;
    size_t nodesSize;
} pig_map_t;


pig_map_t* pig_map_read(FILE* fileptr);

pig_node_t* pig_map_find_node(pig_map_t* map, char* nodeName);

void pig_map_destroy(pig_map_t* map);

pig_node_t* pig_link_end_node(pig_link_t* link);

const char* pig_link_start_node_name(pig_link_t* link);

const char* pig_link_end_node_name(pig_link_t* link);

unsigned int pig_link_distance(pig_link_t* link);

pig_link_t* pig_node_find_link(pig_node_t* node, char* nodeName);

#endif // _PIG_MAP_H
