#ifndef _PATH_ANALYZER_H
#define _PATH_ANALYZER_H

#include <stdint.h>
#include "measurements_reader.h"
#include "pig_path.h"
#include "sensor.h"

measurement_iter_t path_analyze;

typedef struct {
    sensor_t* sensors; // array of sensor_t
    size_t currentMeasure;
    float lastNodeDistance;
    size_t corrosionThreshold;
    uint32_t fluxSpeed;
    uint32_t censusSpeed;
    pig_path_t* path;
} path_analyzer_t;

path_analyzer_t* path_analyzer_create(measurements_reader_t* reader,
                                      pig_path_t* path);

void path_analyzer_destroy(path_analyzer_t* analyzer);

#endif // _PATH_ANALYZER_H
