#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "measurements_reader.h"
#include "path_analyzer.h"
#include "pig_map.h"

#define ARG_MEASUREMENTS_FILE_POS 1
#define ARG_PIPES_FILE_POS 2
#define ARG_PATH_FILE_POS 3

int analyze(char const *measurementsFilename,
            char const *pipesFilename,
            char const *pathFilename) {
    int code = 0;
    FILE* pipesFileptr = NULL;
    FILE* measurementsFileptr = NULL;
    FILE* pathFileptr = NULL;

    if (!(measurementsFileptr = fopen(measurementsFilename, "rb"))) {
        fprintf(stderr, "%s\n", "Archivo inválido");
        return 1;
    }

    pig_map_t* map = NULL;
    if (!(pipesFileptr = fopen(pipesFilename, "r"))) {
        fclose(measurementsFileptr);
        fprintf(stderr, "%s\n", "Archivo inválido");
        return 1;
    } else {
        map = pig_map_read(pipesFileptr);
        fclose(pipesFileptr);
    }

    pig_path_t* path = NULL;
    if (!(pathFileptr = fopen(pathFilename, "r"))) {
        fclose(measurementsFileptr);
        fprintf(stderr, "%s\n", "Archivo inválido");
        return 1;
    } else {
        path = pig_path_create(map, pathFileptr);
        fclose(pathFileptr);
    }

    measurements_reader_t* reader =
        measurements_reader_create(measurementsFileptr);
    path_analyzer_t* analyzer = path_analyzer_create(reader, path);

    if (!measurements_reader_iterate(reader, path_analyze, analyzer)) {
        fprintf(stderr, "%s\n", "Cantidad de muestras incorrectas");
        code = 1;
    }

    fclose(measurementsFileptr);
    measurements_reader_destroy(reader);
    path_analyzer_destroy(analyzer);
    pig_path_destroy(path);
    pig_map_destroy(map);
    return code;
}

int main(int argc, char const *argv[]) {
    if (argc != 4) {
        puts("Uso: ./tp <mediciones> <tuberias> <camino>");
    }

    return analyze(argv[ARG_MEASUREMENTS_FILE_POS],
                   argv[ARG_PIPES_FILE_POS],
                   argv[ARG_PATH_FILE_POS]);
}
