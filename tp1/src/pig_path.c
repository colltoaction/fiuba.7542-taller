#include <stdlib.h>
#include <string.h>
#include "pig_map.h"
#include "pig_path.h"

#define DATA_SIZE_INIT 10
#define DATA_RESIZE_FACTOR 2


pig_link_t* pig_path_next(pig_path_t* path) {
    if (path->curr >= path->length) {
        return NULL;
    }

    return path->data[++path->curr];
}

pig_link_t* pig_path_curr(pig_path_t* path) {
    return path->data[path->curr];
}

void pig_path_generate(pig_path_t* path, pig_node_t* startNode) {
    path->length = 0;
    path->dataSize = DATA_SIZE_INIT;
    path->data = malloc(path->dataSize * sizeof(pig_link_t*));

    char buf[NODE_NAME_MAX_LEN];
    while (fgets(buf, LINE_MAX_LEN, path->fileptr) != NULL &&
           sscanf(buf, "%[^\n]", buf) == 1) {
        if (path->length >= path->dataSize) {
            path->dataSize *= DATA_RESIZE_FACTOR;
            path->data = realloc(path->data,
                                 path->dataSize * sizeof(pig_link_t*));
        }

        path->data[path->length] = pig_node_find_link(startNode, buf);
        startNode = pig_link_end_node(path->data[path->length]);
        path->length++;
    }
}

pig_path_t* pig_path_create(pig_map_t* map, FILE* fileptr) {
    pig_path_t* path = malloc(sizeof(pig_path_t));
    path->curr = 0;
    path->isEnd = false;
    path->fileptr = fileptr;
    char buf[NODE_NAME_MAX_LEN];

    if (fgets(buf, LINE_MAX_LEN, fileptr) != NULL &&
        sscanf(buf, "%[^\n]", buf) == 1) {
        pig_node_t* startNode = pig_map_find_node(map, buf);
        pig_path_generate(path, startNode);
    }

    return path;
}

void pig_path_destroy(pig_path_t* path) {
    free(path->data);
    free(path);
}
