#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "measurements_reader.h"
#include "path_analyzer.h"
#include "sensor.h"

#define MIN_CORROSION_MEASUREMENT 0x00A0
#define HOUR_TO_MIN_FACTOR 60 // 60 min/hour
#define MIN_CORROSION_MEASUREMENT_DISTANCE (0.5f * HOUR_TO_MIN_FACTOR)
#define MIN_LEAK_MEASUREMENT 0x00FF
#define LEAK_FORMAT "RUPTURA %s->%s (%.2fm)\n"
#define CORROSION_FORMAT "CORROSION %s->%s (%.2fm)\n"


float path_analyzer_current_distance(path_analyzer_t* analyzer) {
    return analyzer->currentMeasure *
           analyzer->fluxSpeed /
           (float)analyzer->censusSpeed /
           HOUR_TO_MIN_FACTOR;
}

void path_analyze(measurement_t* const measurements,
                  size_t count,
                  void* extra) {
    path_analyzer_t* analyzer = extra;
    analyzer->currentMeasure++;
    float currentDistance = path_analyzer_current_distance(analyzer);
    pig_link_t* curr = pig_path_curr(analyzer->path);
    float nextDistance = analyzer->lastNodeDistance +
                         pig_link_distance(curr);
    if (nextDistance < currentDistance)
    {
        curr = pig_path_next(analyzer->path);
        analyzer->lastNodeDistance = nextDistance;
    }

    for (size_t i = 0; i < count; ++i) {
        sensor_analyze(analyzer->sensors + i,
                       measurements[i],
                       curr,
                       analyzer->lastNodeDistance,
                       currentDistance);
    }
}

path_analyzer_t* path_analyzer_create(measurements_reader_t* reader,
                                      pig_path_t* path) {
    path_analyzer_t* analyzer = malloc(sizeof(path_analyzer_t));
    analyzer->currentMeasure = 0;
    analyzer->lastNodeDistance = 0.0f;
    analyzer->path = path;
    analyzer->censusSpeed = measurements_reader_census_speed(reader);
    analyzer->fluxSpeed = measurements_reader_flux_speed(reader);
    float corrosionThreshold = ceilf(MIN_CORROSION_MEASUREMENT_DISTANCE *
                                     analyzer->censusSpeed /
                                     analyzer->fluxSpeed);
    size_t sensorCount = measurements_reader_sensor_count(reader);
    analyzer->sensors = malloc(sizeof(sensor_t) * sensorCount);
    for (size_t i = 0; i < sensorCount; ++i) {
        sensor_init(analyzer->sensors + i, corrosionThreshold);
    }

    return analyzer;
}

void path_analyzer_destroy(path_analyzer_t* analyzer) {
    free(analyzer->sensors);
    free(analyzer);
}
