#ifndef _PIG_PATH_H
#define _PIG_PATH_H

#include "pig_map.h"

typedef struct {
    size_t curr;
    bool isEnd;
    FILE* fileptr;
    size_t length;
    size_t dataSize;
    pig_link_t** data;
} pig_path_t;

pig_link_t* pig_path_next(pig_path_t* path);

pig_link_t* pig_path_curr(pig_path_t* path);

void pig_path_generate(pig_path_t* path, pig_node_t* startNode);

pig_path_t* pig_path_create(pig_map_t* map, FILE* fileptr);

void pig_path_destroy(pig_path_t* path);

#endif // _PIG_PATH_H
