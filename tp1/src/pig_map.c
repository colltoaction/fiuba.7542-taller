#include <stdlib.h>
#include <string.h>
#include "pig_map.h"

#define LINE_FORMAT "%[^,],%u,%[^,\n]"
#define NODES_SIZE_INIT 10
#define NODES_RESIZE_FACTOR 2
#define LINKS_SIZE_INIT 10
#define LINKS_RESIZE_FACTOR 2

typedef struct {
    char startNode[NODE_NAME_MAX_LEN];
    unsigned int distance;
    char endNode[NODE_NAME_MAX_LEN];
} line_t;

pig_node_t* pig_map_find_node(pig_map_t* map, char* nodeName) {
    for (size_t i = 0; i < map->nodesLength; ++i) {
        if (strcmp(map->nodes[i]->name, nodeName) == 0) {
            return map->nodes[i];
        }
    }

    return NULL;
}

pig_node_t* pig_map_find_or_create_node(pig_map_t* map, char* nodeName) {
    pig_node_t* node = pig_map_find_node(map, nodeName);
    if (node != NULL) {
        return node;
    }

    if (map->nodesLength >= map->nodesSize) {
        map->nodesSize *= NODES_RESIZE_FACTOR;
        size_t newSize = map->nodesSize * sizeof(pig_node_t*);
        map->nodes = realloc(map->nodes, newSize);
    }

    node = malloc(sizeof(pig_node_t));
    node->links = malloc(LINKS_SIZE_INIT * sizeof(pig_link_t*));
    node->linksSize = LINKS_SIZE_INIT;
    node->linksLength = 0;
    memcpy(node->name, nodeName, NODE_NAME_MAX_LEN);
    return (map->nodes[map->nodesLength++] = node);
}

void pig_node_add_link(pig_node_t* startNode,
                       pig_node_t* endNode,
                       unsigned int distance) {
    if (startNode->linksLength >= startNode->linksSize) {
        startNode->linksSize *= LINKS_RESIZE_FACTOR;
        size_t newSize = startNode->linksSize * sizeof(pig_link_t*);
        startNode->links = realloc(startNode->links, newSize);
    }

    pig_link_t* linkStart = malloc(sizeof(pig_link_t));
    linkStart->startNode = startNode;
    linkStart->endNode = endNode;
    linkStart->distance = distance;
    startNode->links[startNode->linksLength++] = linkStart;
}

void pig_map_add_line(pig_map_t* map, line_t* line) {
    pig_node_t* startNode = pig_map_find_or_create_node(map, line->startNode);
    pig_node_t* endNode = pig_map_find_or_create_node(map, line->endNode);
    pig_node_add_link(startNode, endNode, line->distance);
    pig_node_add_link(endNode, startNode, line->distance);
}

pig_map_t* pig_map_read(FILE* fileptr) {
    pig_map_t* map = malloc(sizeof(pig_map_t));
    map->nodes = malloc(NODES_SIZE_INIT * sizeof(pig_node_t*));
    map->nodesSize = NODES_SIZE_INIT;
    map->nodesLength = 0;
    line_t p;
    char buf[LINE_MAX_LEN];
    while (fgets(buf, LINE_MAX_LEN, fileptr) != NULL &&
           sscanf(buf, LINE_FORMAT, p.startNode, &p.distance, p.endNode) == 3) {
        pig_map_add_line(map, &p);
    }

    return map;
}

void pig_link_destroy(pig_link_t* link) {
    free(link);
}

pig_node_t* pig_link_end_node(pig_link_t* link) {
    return link->endNode;
}

const char* pig_link_start_node_name(pig_link_t* link) {
    return link->startNode->name;
}

const char* pig_link_end_node_name(pig_link_t* link) {
    return link->endNode->name;
}

unsigned int pig_link_distance(pig_link_t* link) {
    return link->distance;
}

void pig_node_destroy(pig_node_t* node) {
    for (size_t i = 0; i < node->linksLength; ++i) {
        pig_link_destroy(node->links[i]);
    }

    free(node->links);
    free(node);
}

void pig_map_destroy(pig_map_t* map) {
    for (size_t i = 0; i < map->nodesLength; ++i) {
        pig_node_destroy(map->nodes[i]);
    }

    free(map->nodes);
    free(map);
}

pig_link_t* pig_node_find_link(pig_node_t* node, char* nodeName) {
    for (size_t i = 0; i < node->linksLength; ++i) {
        if (strcmp(node->links[i]->endNode->name, nodeName) == 0) {
            return node->links[i];
        }
    }

    // Malformed path file, can't happen
    return NULL;
}
