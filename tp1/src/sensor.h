#ifndef _SENSOR_H
#define _SENSOR_H

#include "measurements_reader.h"
#include "pig_map.h"

typedef struct {
    float corrosionThreshold;
    pig_link_t* corrosionStartLink;
    float corrosionStart;
    size_t corrosionCount;
} sensor_t;

void sensor_init(sensor_t* sensor, float corrosionThreshold);

void sensor_analyze(sensor_t* sensor,
                    measurement_t const measurement,
                    pig_link_t* curr,
                    float lastNodeDistance,
                    float currentDistance);

#endif // _SENSOR_H
