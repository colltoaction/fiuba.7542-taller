#include "sensor.h"

#define MIN_CORROSION_MEASUREMENT 0x00A0
#define MIN_LEAK_MEASUREMENT 0x00FF
#define LEAK_FORMAT "RUPTURA %s->%s (%.2fm)\n"
#define CORROSION_FORMAT "CORROSION %s->%s (%.2fm)\n"


bool sensor_detected_corrosion(sensor_t* sensor, size_t corrosionThreshold) {
    return sensor->corrosionCount >= corrosionThreshold;
}

void sensor_add_corrosion(sensor_t* sensor,
                          pig_link_t* corrosionStartLink,
                          float currentDistance) {
    if (sensor->corrosionCount == 0) {
        sensor->corrosionStartLink = corrosionStartLink;
        sensor->corrosionStart = currentDistance;
    }

    sensor->corrosionCount++;
}

void sensor_clear_corrosion(sensor_t* sensor) {
    sensor->corrosionStartLink = NULL;
    sensor->corrosionStart = 0.0f;
    sensor->corrosionCount = 0;
}

void sensor_init(sensor_t* sensor, float corrosionThreshold) {
    sensor->corrosionThreshold = corrosionThreshold;
    sensor_clear_corrosion(sensor);
}

void sensor_print_corrosion(sensor_t* sensor) {
    printf(CORROSION_FORMAT,
           pig_link_start_node_name(sensor->corrosionStartLink),
           pig_link_end_node_name(sensor->corrosionStartLink),
           sensor->corrosionStart);
}

void sensor_analyze(sensor_t* sensor,
                    measurement_t const measurement,
                    pig_link_t* curr,
                    float lastNodeDistance,
                    float currentDistance) {
    if (measurement >= MIN_LEAK_MEASUREMENT) {
        printf(LEAK_FORMAT,
               pig_link_start_node_name(curr),
               pig_link_end_node_name(curr),
               currentDistance - lastNodeDistance);
    } else if (measurement >= MIN_CORROSION_MEASUREMENT) {
        sensor_add_corrosion(sensor,
                             curr,
                             currentDistance - lastNodeDistance);
        if (sensor_detected_corrosion(sensor,
                                      sensor->corrosionThreshold)) {
            sensor_print_corrosion(sensor);
            sensor_clear_corrosion(sensor);
        }
    } else {
        sensor_clear_corrosion(sensor);
    }
}
