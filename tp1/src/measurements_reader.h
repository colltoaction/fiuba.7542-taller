#ifndef _MEASUREMENTS_READER_H
#define _MEASUREMENTS_READER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef uint16_t measurement_t;

/**
 * Tipo de la función que se espera recibir por parámetro en la función
 * measurements_reader_read, que internamente itera las mediciones.
 */
typedef void measurement_iter_t(measurement_t * const, size_t, void*);

typedef struct {
    uint32_t fluxSpeed;
    uint32_t censusSpeed;
    uint32_t sensorCount;
} measurements_header_t;

typedef struct {
    measurements_header_t header;
    FILE* fileptr;
} measurements_reader_t;

measurements_reader_t* measurements_reader_create(FILE* fileptr);

/**
 * Itera las mediciones de forma secuencial, evitando cargar en memoria todo el
 * archivo. Recibe una función de tipo measurement_iter_t a la que se le pasará
 * por parámetro un array con las medidas de todos los sensores en un
 * determinado tiempo t, la cantidad de sensores, y un parámetro extra que es
 * el mismo que recibe la función.
 *
 * @return falso si el archivo estaba mal formado, verdadero en caso contrario.
 */
bool measurements_reader_iterate(measurements_reader_t* reader,
                                 measurement_iter_t iter,
                                 void* extra);

uint32_t measurements_reader_census_speed(measurements_reader_t* reader);

uint32_t measurements_reader_flux_speed(measurements_reader_t* reader);

uint32_t measurements_reader_sensor_count(measurements_reader_t* reader);

void measurements_reader_destroy(measurements_reader_t* measurements);

#endif // _MEASUREMENTS_READER_H
